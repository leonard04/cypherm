import 'dart:ui';

import 'package:cool_alert/cool_alert.dart';
import 'package:cypherm/modal/ClientList.dart';
import 'package:cypherm/modal/UserList.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:progress_dialog/progress_dialog.dart';
import 'modal/LeadsList.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

import 'modal/SessionPage.dart';

class LeadsAdd extends StatefulWidget {
  String ip = "";
  String companyId = "";
  final VoidCallback reload;
  LeadsAdd({Key key, this.ip, this.companyId, int id, this.reload}) : super(key: key);

  @override
  _LeadsAddState createState() => _LeadsAddState();
}

class _LeadsAddState extends State<LeadsAdd> {
  ProgressDialog pr;
  SessionManager pref = SessionManager();
  String username = "";
  String project = "ritz";
  // String project = "cypher4";
  var loading = false;
  TextEditingController name = new TextEditingController();
  TextEditingController description = new TextEditingController();

  String _valmp = 'Choose Client';
  String _value = "";
  static List<UserRequest> _partner = [];
  static List<UserRequest> _referral = [];
  var _selectedPartner = [];
  var _selectedReferral = [];
  List<ClientRequest> newClient = [];

  List dx = List();
  String _valClient;
  List dy = List();
  String _valCategory;

  List<dynamic> _dataProvince = List();

  Future getClient() async {
    dx.clear();
    setState(() {
      loading = true;
    });

    var url = "http://" +
        widget.ip +
        "/" +
        project +
        "/public/api/leads/clients/" +
        widget.companyId;
    final response = await http.get(url);
    if (response.contentLength == 2) {
    } else {
      var data = jsonDecode(response.body);
      setState(() {
        dx = data['data'];
        loading = false;
      });
    }
    print(dx);
  }

  Future getCategory() async {
    dy.clear();
    setState(() {
      loading = true;
    });

    var url = "http://" +
        widget.ip +
        "/" +
        project +
        "/public/api/leads/category/" +
        widget.companyId;
    final response = await http.get(url);
    if (response.contentLength == 2) {
    } else {
      var data = jsonDecode(response.body);
      setState(() {
        dy = data['data'];
        loading = false;
      });
    }
    print(dy);
  }

  getReferral() async {
    _referral.clear();
    setState(() {
      loading = true;
    });

    var url = "http://" +
        widget.ip +
        "/" +
        project +
        "/public/api/leads/users/" +
        widget.companyId;
    final response = await http.get(url);
    if (response.contentLength == 2) {
    } else {
      final data = jsonDecode(response.body)['data'];
      data.forEach((api) {

        _referral.add(new UserRequest(api['id'], api['company_id'], api['name'],
            api['email'], api['username'], widget.ip));
      });
      setState(() {
        loading = false;
      });
    }
  }

  getPartner() async {
    _partner.clear();
    setState(() {
      loading = true;
    });
    var url = "http://" +
        widget.ip +
        "/" +
        project +
        "/public/api/leads/users/" +
        widget.companyId;
    final response = await http.get(url);
    if (response.contentLength == 2) {
    } else {
      final data = jsonDecode(response.body)['data'];
      data.forEach((api) {
        _partner.add(new UserRequest(api['id'], api['company_id'], api['name'],
            api['email'], api['username'], widget.ip));
      });
      setState(() {
        loading = false;
      });
    }
  }

  final _itemPartners = _partner
      .map((partner) => MultiSelectItem<UserRequest>(partner, partner.name))
      .toList();

  final _itemReferrrals = _referral
      .map((referral) => MultiSelectItem<UserRequest>(referral, referral.name))
      .toList();
  @override
  void initState() {
    Future<String> authToken = pref.getUsername();
    authToken.then((data) {
      setState(() {
        username = data.toString();
      });
    }, onError: (e) {
      print(e);
    });
    super.initState();
    getReferral();
    getPartner();
    getClient();
    getCategory();
    // _selectedPartner = _partner;
    // _selectedReferral = _referral;
    setState(() {
      _selectedPartner = _partner;
      _selectedReferral = _referral;
      // username = pref.getUsername().toString();
    });
  }

  Future _submitLeadsData() async {
    // print(name.text.toString());
    // print(description.text.toString());
    // print(widget.companyId);
    // print(_selectedPartner.toString());
    // print(_selectedReferral.toString());
    // print(username.toString());
    String project = "ritz";
    // String project = "cypher4";
    await pr.show();
    String url = "http://" + widget.ip + "/"+project+"/public/api/leads/add";
    final response = await http.post(
      url,
      body: json.encode({
        "leads_name": name.text.toString(),
        "description":description.text.toString(),
        "client": _valClient,
        "category": _valCategory,
        "progress": 0,
        "company_id": widget.companyId,
        "created_by": username.toString(),
        "partner":_selectedPartner.toString(),
        "referral": _selectedReferral.toString()
      }),
      headers: {'Content-Type': 'application/json'},
    );
    var res = json.decode(response.body);
    print(res);
    if (res['success'] == 0) {
      await CoolAlert.show(
        context: context,
        type: CoolAlertType.error,
        title: "Error",
        text: res['message'].toString(),

      );

    } else {
      await CoolAlert.show(
        context: context,
        type: CoolAlertType.success,
        title: "",
        text: res['message'].toString(),
      );
    }
    widget.reload();
    Navigator.pop(context);
    await pr.hide();

  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, showLogs: true);
    pr.style(message: 'Please wait...');
    pr = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    // TODO: implement build
    return Scaffold(
        appBar: new AppBar(
          leading: IconButton(
            icon: Icon(CupertinoIcons.back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          //warna background
          toolbarHeight: 70.0,
          backgroundColor: Colors.orange[600],
          //judul
          centerTitle: true, // this is all you need
          title: new Text("Add Leads"),

          //bottom
        ),
        body: loading
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                padding: const EdgeInsets.all(20.0),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black38,
                          blurRadius: 7,
                        ),
                      ],
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      color: Colors.white,
                      border: Border.all(
                          style: BorderStyle.solid, color: Colors.white)),
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[

                      TextField(
                        controller: name,
                        maxLines: 1,
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.orange, width: 2.0),
                              borderRadius: const BorderRadius.all(
                                  const Radius.circular(50.0))),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.orange[700], width: 2.0),
                              borderRadius: const BorderRadius.all(
                                  const Radius.circular(50.0))),
                          hintText: "Leads Name",
                          hintStyle: TextStyle(color: Colors.orange[700]),
                          border: new OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(50.0),
                            ),
                            borderSide: new BorderSide(
                                color: Colors.orange[800],
                                width: 2.0,
                                style: BorderStyle.solid),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      MultiSelectDialogField(
                        items: _itemPartners,
                        title: Text("Partner"),
                        selectedColor: Colors.orange,
                        decoration: BoxDecoration(
                          color: Colors.orange.withOpacity(0.1),
                          borderRadius: BorderRadius.all(Radius.circular(40)),
                          border: Border.all(
                            color: Colors.orange,
                            width: 2,
                          ),
                        ),
                        buttonIcon: Icon(
                          CupertinoIcons.person_2,
                          color: Colors.orange,
                        ),
                        buttonText: Text(
                          "Choose Partner",
                          style: TextStyle(
                            color: Colors.orange[800],
                            fontSize: 16,
                          ),
                        ),
                        onConfirm: (results) {
                          _selectedPartner = results;
                        },
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      MultiSelectDialogField(
                        items: _itemReferrrals,
                        title: Text("Referral"),
                        selectedColor: Colors.orange,
                        decoration: BoxDecoration(
                          color: Colors.orange.withOpacity(0.1),
                          borderRadius: BorderRadius.all(Radius.circular(40)),
                          border: Border.all(
                            color: Colors.orange,
                            width: 2,
                          ),
                        ),
                        buttonIcon: Icon(
                          CupertinoIcons.person_3,
                          color: Colors.orange,
                        ),
                        buttonText: Text(
                          "Choose Referral",
                          style: TextStyle(
                            color: Colors.orange[800],
                            fontSize: 16,
                          ),
                        ),
                        onConfirm: (results) {
                          _selectedReferral = results;
                        },
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Container(
                        decoration:  ShapeDecoration(
                          color: Colors.orange.withOpacity(0.1),
                          shape: RoundedRectangleBorder(
                            side: BorderSide(width: 1.0, style: BorderStyle.solid,color: Colors.orange[800]),
                            borderRadius: BorderRadius.all(Radius.circular(30.0)),
                          ),
                        ),
                        child: DropdownButton(
                          isExpanded: true,
                          hint: Text(
                              "Choose Client",
                            style: TextStyle(color: Colors.orange[800]),
                          ),
                          value: _valClient,
                          items: dx.map((item) {
                            return DropdownMenuItem(
                              child: Text(
                                item['company_name'].toString(),
                                style: TextStyle(color: Colors.orange[800]),
                              ),
                              value: item['id'].toString(),
                            );
                          }).toList(),
                          onChanged: (value) {
                            setState(() {
                              _valClient = value;
                              print(_valClient);
                            });
                          },
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Container(
                        decoration:  ShapeDecoration(
                          color: Colors.orange.withOpacity(0.1),
                          shape: RoundedRectangleBorder(
                            side: BorderSide(width: 1.0, style: BorderStyle.solid,color: Colors.orange[800]),
                            borderRadius: BorderRadius.all(Radius.circular(30.0)),
                          ),
                        ),
                        child: DropdownButton(
                          isExpanded: true,
                          hint: Text(
                              "Choose Category",
                              style: TextStyle(color: Colors.orange[800]),
                          ),
                          value: _valCategory,
                          items: dy.map((item) {
                            return DropdownMenuItem(
                              child: Text(
                                item['category_name'].toString(),
                                style: TextStyle(color: Colors.orange[800]),
                              ),
                              value: item['id'].toString(),
                            );
                          }).toList(),
                          onChanged: (value) {
                            setState(() {
                              _valCategory = value;
                              print(_valCategory);
                            });
                          },
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      TextField(
                        controller: description,
                        maxLines: 8,
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide:
                              BorderSide(color: Colors.orange, width: 2.0),
                              borderRadius: const BorderRadius.all(
                                  const Radius.circular(15.0))),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.orange[700], width: 2.0),
                              borderRadius: const BorderRadius.all(
                                  const Radius.circular(15.0))),
                          hintText: "Leads Description",
                          hintStyle: TextStyle(color: Colors.orange[700]),
                          border: new OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(50.0),
                            ),
                            borderSide: new BorderSide(
                                color: Colors.orange[800],
                                width: 2.0,
                                style: BorderStyle.solid),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 60.0,
                        child: RaisedButton(
                          color: Colors.orange[800],
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          ),
                          child: Text(
                            "Save",
                            style: TextStyle(color: Colors.white, fontSize: 25.0),
                          ),
                          onPressed: () {
                            CoolAlert.show(
                                context: context,
                                type: CoolAlertType.confirm,
                                text: "Do you want to save?",
                                confirmBtnText: "Yes",
                                cancelBtnText: "No",
                                confirmBtnColor: Colors.green,
                                onConfirmBtnTap: (){
                                  _submitLeadsData();
                                }
                            );
                            // _submitData();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              )
        // :

        );
  }
}
