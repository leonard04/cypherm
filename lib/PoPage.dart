import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'modal/PoList.dart';
import 'dart:convert';
import 'PoDtlPage.dart';

// ignore: must_be_immutable
class PoList extends StatefulWidget {
  String ip = "";
  String companyId = "";
  PoList({Key key, this.ip, this.companyId, int id}) : super(key: key);

  @override
  _PoListState createState() => _PoListState();
}

class _PoListState extends State<PoList> {
  // String project = "ritz";
  String project = "cypher4";
  var loading = false;
  final list = new List<PoRequest>();

   _lihatData() async {
    list.clear();
    setState(() {
      loading = true;
    });
    var url =
        "http://" + widget.ip + "/"+project+"/public/api/po/" + widget.companyId;
    final response = await http.get(url);
    if (response.contentLength == 2) {
    } else {
      final data = jsonDecode(response.body)['data'];
      data.forEach((api) {
        final ab = new PoRequest(api['id'], api['po_num'], api['supplier_name'],
            api['project_name'], api['approved_by'], api['approved_time'],api['company_id'],widget.ip);
        list.add(ab);
      });
      setState(() {
        loading = false;
      });
      // print(list);
      // print(list.length);

    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _lihatData();
}


  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: new AppBar(
          //warna background
          leading: IconButton(
            icon: Icon(CupertinoIcons.back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          toolbarHeight: 70.0,
          backgroundColor: Colors.orange[600],
          //judul
          centerTitle: true, // this is all you need
          title: new Text("Purchase Order"),
        ),
        body: loading
            ? Center(child: CircularProgressIndicator())
            : (list.length <= 0)?
            Container(
              padding: EdgeInsets.all(10),
                child: GestureDetector(
                  onTap: (){

                  // print("Tap "+poItem.ip);
                },
                child: Card(
                  elevation: 10,
                  child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                      child: ListTile(
                        leading: Container(
                          width: 10.0,
                          color: Colors.black12,
                        ),
                        title:  Text(
                          "No Record Found",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              fontSize: 18,
                              fontStyle: FontStyle.italic
                          ),
                        ),
                        subtitle:  Text(
                          "",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 16
                          ),
                        ),

                      )
                  ),
                ),
          ),

        ) :
        ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index){
            PoRequest poList = list[index];
            return Container(
              padding: EdgeInsets.all(10),
              child: GestureDetector(
                onTap: (){
                  if(poList.approved_by.toString() == "null"){
                    // print("Belum Diapprove");

                    Navigator.push(context, new MaterialPageRoute(
                        builder: (context) => PoDtl(
                          ip: widget.ip,
                          companyId: poList.company_id.toString(),
                          id: poList.id,
                          po_num: poList.po_num.toString(),
                          reload: _lihatData,
                        ))
                    );
                  } else {
                    CoolAlert.show(
                      context: context,
                      type: CoolAlertType.info,
                      text: "This PO has been approved!",
                    );
                  }
                  // print("Tap "+poItem.ip);
                },
                child: Card(
                  elevation: 10,
                  child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                      child: ListTile(
                        leading: Container(
                          width: 10.0,
                          color: (poList.approved_by.toString() == "null") ? Colors.amber : Colors.greenAccent,
                        ),
                        title:  Text(
                          (poList.po_num.toString() != "null")?poList.po_num.toString():"N/A",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 18
                          ),
                        ),
                        subtitle:  Text(
                          (poList.supplier_name.toString() != "null")?poList.supplier_name.toString():"-",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 16
                          ),
                        ),

                      )
                  ),
                ),
              ),

            );
          },
        ),
    );
  }



}
