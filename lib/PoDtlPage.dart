import 'dart:convert';
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import './modal/SessionPage.dart';
import 'package:http/http.dart' as http;
import 'package:cool_alert/cool_alert.dart';
import 'package:progress_dialog/progress_dialog.dart';
import "package:intl/intl.dart";
import 'package:html/parser.dart';

// ignore: must_be_immutable
class PoDtl extends StatefulWidget {
  // ignore: non_constant_identifier_names
  String ip, companyId, po_num;
  int id;
  final VoidCallback reload;
  // ignore: non_constant_identifier_names
  PoDtl({Key key, this.ip, this.companyId, this.id, this.reload, this.po_num})
      : super(key: key);

  @override
  _PoDtlState createState() => _PoDtlState();
}

class _PoDtlState extends State<PoDtl> {
  SessionManager pref = SessionManager();
  double Tital = 0;
  // initialize var
  ProgressDialog pr;
  TextEditingController idPo = new TextEditingController();
  TextEditingController notes = new TextEditingController();
  String msg = '';
  // ignore: non_constant_identifier_names
  String companyId = "", supplier_address = "";
  String username = "", request_by = "", supplier_name = "", payment_term;
  final list = new List<getDtl>();
  List lists = new List();
  List products = new List();
  List totals = new List();
  @override
  void initState() {
    Future<String> authToken = pref.getUsername();
    authToken.then((data) {
      setState(() {
        username = data.toString();
      });
    }, onError: (e) {
      print(e);
    });
    _ambilDtl();
    super.initState();
  }

  String _parseHtmlString(String htmlString) {
    final document = parse(htmlString);
    final String parsedString = parse(document.body.text).documentElement.text;

    return parsedString;
  }
  
  _ambilDtl() async {
    // String project = "ritz";
    String project = "cypher4";
    try {
      String url = "http://" +
          widget.ip +
          "/"+project+"/public/api/po/detail/" +
          widget.companyId +
          "/" +
          widget.id.toString();
      final response = await http.get(url);
      if (response.contentLength == 2) {
      } else {
        list.clear();
        final data = jsonDecode(response.body)['data'];
        //print(data);
        // print(data['po_detail']);
        setState(() {
          request_by = data['po']['request_by'].toString();
          supplier_name = data['po']['supplier_name'].toString();
          payment_term = data['po']['payment_term'].toString();
          companyId = data['po']['company_id'].toString();
          supplier_address = (data['po']['supplier_address'].toString().contains("<p>"))?_parseHtmlString(data['po']['supplier_address'].toString()):data['po']['supplier_address'].toString();
        });


        var d2 = data['po_detail'];
        for (final i in d2) {
          var productMap = {
            'id': i['id'],
            'name': i['item_name'],
            'price': i['price'],
            'quantity': i['qty'],
            'item_id': i['item_id'],
            'item_uom': i['item_uom'],
          };

          var ix = double.parse(i['price'].toString()) * double.parse(i['qty'].toString());
          setState(() {
            Tital = Tital + ix ;
          });

          products.add(productMap);
        }


        var productMapTotal = {
          'id': "",
          'name':"Total",
          'price': Tital,
          'quantity': "",
          'item_id': "",
          'item_uom':"",
        };
        products.add(productMapTotal);


        print(products);

        // print(list);

        // debugPrint('$list');
      }
    } catch (e) {}
  }

  Future _submitData() async {
    // String project = "ritz";
    String project = "cypher4";
    await pr.show();
    String url = "http://" + widget.ip + "/"+project+"/public/api/po/approve";
    final response = await http.post(
      url,
      body: json.encode({
        "id": widget.id.toString(),
        "username": username.toString(),
        "notes": notes.text.toString()
      }),
      headers: {'Content-Type': 'application/json'},
    );
    var res = json.decode(response.body);
    print(res);
    if (res['success'] == 0) {
      await CoolAlert.show(
        context: context,
        type: CoolAlertType.error,
        title: "Error",
        text: res['message'].toString(),

      );

    } else {
      await CoolAlert.show(
        context: context,
        type: CoolAlertType.success,
        title: "",
        text: res['message'].toString(),
      );
    }
    widget.reload();
    Navigator.pop(context);
    await pr.hide();
  }

  @override
  Widget build(BuildContext context) {
    // final List<>
    // debugPrint('movieTitle: $_ambilDtl()');
    final currencyFormatter = NumberFormat("#,##0.00", "en_US");
    pr = new ProgressDialog(context, showLogs: true);
    pr.style(message: 'Please wait...');
    pr = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    final data = MediaQuery.of(context);
    int total = 0;
    double wt = 80.0;
    double wq = 80.0;
    double wa = 30.0;

    return Scaffold(
        // backgroundColor: Color.fromRGBO(252, 241, 241, 1),
        appBar: new AppBar(
          leading: IconButton(
            icon: Icon(CupertinoIcons.back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          //warna background
          toolbarHeight: 70.0,
          backgroundColor: Colors.orange[600],
          //judul
          centerTitle: true, // this is all you need
          title: new Text("Purchase Order Detail\n" + widget.po_num.toString()),

          //bottom
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(30.0),
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: 100.0,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black38,
                        blurRadius: 10,
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    color: Colors.white,
                    border: Border.all(
                        style: BorderStyle.solid, color: Colors.white)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        children: [
                          Text(
                            "Request By:",
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                          ),
                          Text(
                            (request_by.toString() != "null")?request_by.toString():"N/A",
                            style:
                                TextStyle(color: Colors.black, fontSize: 25.0),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(right: 10.0),
                      width: 140.0,
                      height: 50.0,
                      child: RaisedButton(
                        color: Colors.orange,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        ),
                        child: Text(
                          "Approve",
                          style: TextStyle(color: Colors.white, fontSize: 25.0),
                        ),
                        onPressed: () {
                          CoolAlert.show(
                            context: context,
                            type: CoolAlertType.confirm,
                            text: "Do you want to approve?",
                            confirmBtnText: "Yes",
                            cancelBtnText: "No",
                            confirmBtnColor: Colors.green,
                            onConfirmBtnTap: (){
                              _submitData();
                            }
                          );
                          // _submitData();
                        },
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
              ),

              Container(
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black38,
                        blurRadius: 10,
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    color: Colors.white,
                    border: Border.all(
                        style: BorderStyle.solid, color: Colors.white)
                ),
                padding: const EdgeInsets.all(10.0),
                width: MediaQuery.of(context).size.width,

                child: Column(
                  children: [
                    Text(
                      "From Vendor",
                      style: TextStyle(fontSize: 25.0),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                    ),
                    ListTile(
                      leading: Icon(Icons.house_rounded),
                      title: Text(
                        supplier_name.toString(),
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        supplier_address.toString(),
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                        ),
                      ),

                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
              ),
              Container(
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black38,
                        blurRadius: 10,
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    color: Colors.white,
                    border: Border.all(
                        style: BorderStyle.solid, color: Colors.white)
                    ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                    ),
                    Text(
                      "Summary",
                      style: TextStyle(fontSize: 25.0),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0),
                    ),
                    SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: DataTable(
                              columns: [
                                DataColumn(label: Text("Qty"), tooltip: "Quantity"),
                                DataColumn(label: Text("Item Name"), tooltip: "Item Name"),
                                DataColumn(label: Text("Price (IDR)"), tooltip: "Price per item"),
                              ],
                              rows: products.map((e) {
                                double d = double.parse(e['price'].toString());
                                return DataRow(cells: [
                                  DataCell(Container(
                                    width: wa,
                                    child: Text(
                                        (e['item_uom'].toString() != "null")?e['quantity'].toString()+" "+e['item_uom'].toString():e['quantity'].toString()+" "+"N/A",
                                        style: TextStyle(fontSize: 12.5)
                                    ),
                                  )),
                                  DataCell(Container(
                                    width: wq,
                                    child: Text(
                                        (e['name'].toString() != "null")?e['name'].toString():"N/A",
                                        style: (e['name'].toString() == "Total")? TextStyle(fontSize: 13, fontWeight: FontWeight.bold) :TextStyle(fontSize: 12.5)),
                                  )),
                                  DataCell(Container(
                                    width: wt,
                                    child: Text(currencyFormatter.format(d).toString(), style: (e['name'].toString() == "Total")? TextStyle(fontSize: 12, fontWeight: FontWeight.bold) :TextStyle(fontSize: 12.5), textAlign: TextAlign.right),
                                  ))
                                ]);
                              }).toList()),

                        )
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
              ),
              Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Payment :",
                        style: TextStyle(
                          color: Colors.black,
                            fontSize: 16
                        ),
                      ),
                      Text(
                        payment_term.toString(),
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16
                        ),
                      )
                    ],
                  ),
                  padding: const EdgeInsets.all(10.0),
                  width: MediaQuery.of(context).size.width,
                  height: 50.0,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black38,
                        blurRadius: 10,
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    color: Colors.white,
                    border: Border.all(
                        style: BorderStyle.solid, color: Colors.white)
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
              ),
              TextField(
                controller: notes,
                maxLines: 8,
                decoration: InputDecoration(
                  hintText: "Notes...",
                  border: new OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(0.0),
                    ),
                    borderSide: new BorderSide(
                      color: Colors.black,
                      width: 1.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}

// ignore: camel_case_types
class getDtl {
  getDtl(this.id, this.qty, this.price, this.item_name, this.item_id, this.item_uom);

  // ignore: non_constant_identifier_names
  final int id;
  // ignore: non_constant_identifier_names
  final String qty;

  // ignore: non_constant_identifier_names
  final String item_id;
  final String item_uom;

  final String price;
  // ignore: non_constant_identifier_names
  final String item_name;
}
