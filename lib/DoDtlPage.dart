import 'dart:convert';
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import './modal/SessionPage.dart';
import 'package:http/http.dart' as http;
import 'package:cool_alert/cool_alert.dart';
import 'package:progress_dialog/progress_dialog.dart';
import "package:intl/intl.dart";
import 'package:html/parser.dart';


// ignore: must_be_immutable
class DoDtl extends StatefulWidget {
  // ignore: non_constant_identifier_names
  String ip, companyId, do_num,status;
  int id;
  final VoidCallback reload;
  // ignore: non_constant_identifier_names
  DoDtl({Key key, this.ip, this.companyId, this.id, this.reload, this.do_num, this.status})
      : super(key: key);

  @override
  _DoDtlState createState() => _DoDtlState();
}


class _DoDtlState extends State<DoDtl>
{// initialize var
  ProgressDialog pr;
  // ignore: non_constant_identifier_names
  String companyId = "", supplier_address = "";
  String username = "", request_by = "", supplier_name = "", payment_term;
  SessionManager pref = SessionManager();
  List products = new List();
  TextEditingController notes = new TextEditingController();
  final list = new List<getDtl>();


  @override
  void initState() {
    // print(widget.status);
    // print(widget.companyId);
    // print(widget.ip);
    Future<String> authToken = pref.getUsername();
    authToken.then((data) {
      setState(() {
        username = data.toString();
      });
    }, onError: (e) {
      print(e);
    });
    _ambilDtl();
    super.initState();
  }
  _ambilDtl() async {

    // String project = "ritz";
    String project = "cypher4";
    String url = "http://" +
        widget.ip +
        "/"+project+"/public/api/do/detail/" +
        widget.companyId +
        "/" +
        widget.id.toString();
    // print(url);
    try {

      final response = await http.get(url);
      if (response.contentLength == 2) {
      } else {
        list.clear();
        final data = jsonDecode(response.body)['data'];

        setState(() {
          request_by = data['do']['deliver_by'].toString();
          supplier_name = data['do']['division'].toString();
          payment_term = data['do']['notes'].toString();
          companyId = data['do']['company_id'].toString();
          supplier_address = "From "+data['do']['nameFrom'].toString()+" to "+data['do']['nameTo'].toString();
        });

        var d2 = data['do_detail'];
        for (final i in d2) {
          var productMap = {
            'id': i['id'],
            'name': i['item_name'],
            'type': i['type'],
            'quantity': i['qty'],
            'item_id': i['item_id'],
            'item_uom': i['item_uom'],
          };


          products.add(productMap);
        }

      }
    } catch (e) {}
  }

  Future _submitData() async {
    // print(widget.companyId);
    // print(username);
    // print(widget.id);
    // print(notes.text.toString());

    String project = "cypher4";
    await pr.show();
    String url = "http://" + widget.ip + "/"+project+"/public/api/do/approve";
    final response = await http.post(
      url,
      body: json.encode({
        "id_do": widget.id.toString(),
        "username": username.toString(),
        "notes": notes.text.toString()
      }),
      headers: {'Content-Type': 'application/json'},
    );
    var res = json.decode(response.body);
    print(res);
    if (res['success'] == 0) {
      await CoolAlert.show(
        context: context,
        type: CoolAlertType.error,
        title: "Error",
        text: res['message'].toString(),

      );

    } else {
      await CoolAlert.show(
        context: context,
        type: CoolAlertType.success,
        title: "",
        text: res['message'].toString(),
      );
    }
    widget.reload();
    // Navigator.pop(context);
    await pr.hide();

  }

  @override
  Widget build(BuildContext context) {

    pr = new ProgressDialog(context, showLogs: true);
    pr.style(message: 'Please wait...');
    pr = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    final data = MediaQuery.of(context);
    int total = 0;
    double wt = 50.0;
    double wq = 90.0;
    double wa = 45.0;

    return Scaffold(
      // backgroundColor: Color.fromRGBO(252, 241, 241, 1),
        appBar: new AppBar(
          leading: IconButton(
            icon: Icon(CupertinoIcons.back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          //warna background
          toolbarHeight: 70.0,
          backgroundColor: Colors.orange[600],
          //judul
          centerTitle: true, // this is all you need
          title: new Text("Delivery Order Detail\n" + widget.do_num.toString()),

          //bottom
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(30.0),
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: 100.0,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black38,
                        blurRadius: 10,
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    color: Colors.white,
                    border: Border.all(
                        style: BorderStyle.solid, color: Colors.white)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        children: [
                          Text(
                            "Deliver By:",
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                          ),
                          Text(
                            (request_by.toString() != "null")?request_by.toString():"N/A",
                            style:
                            TextStyle(color: Colors.black, fontSize: 25.0),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(right: 10.0),
                      width: 140.0,
                      height: 50.0,
                      child: RaisedButton(
                        color: (widget.status == "delivered")?Colors.green:Colors.orange,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        ),
                        child: Icon((widget.status == "delivered")?CupertinoIcons.checkmark_alt_circle_fill:CupertinoIcons.clock_solid,size: 40.0, color: Colors.white),

                        onPressed: () {
                          if(widget.status=="delivered"){
                            CoolAlert.show(
                              context: context,
                              type: CoolAlertType.info,
                              text: "This DO has been approved!",
                            );
                          } else {
                            CoolAlert.show(
                                context: context,
                                type: CoolAlertType.confirm,
                                text: "Do you want to approve?",
                                confirmBtnText: "Yes",
                                cancelBtnText: "Cancel",
                                confirmBtnColor: Colors.green,
                                onConfirmBtnTap: (){
                                  _submitData();
                                }
                            );
                          }
                        },
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
              ),

              Container(
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black38,
                        blurRadius: 10,
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    color: Colors.white,
                    border: Border.all(
                        style: BorderStyle.solid, color: Colors.white)
                ),
                padding: const EdgeInsets.all(10.0),
                width: MediaQuery.of(context).size.width,

                child: Column(
                  children: [
                    Text(
                      "Division",
                      style: TextStyle(fontSize: 25.0),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                    ),
                    ListTile(
                      leading: Icon(Icons.house_rounded),
                      title: Text(
                        supplier_name.toString(),
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        supplier_address.toString(),
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                        ),
                      ),

                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
              ),
              Container(
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black38,
                        blurRadius: 10,
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    color: Colors.white,
                    border: Border.all(
                        style: BorderStyle.solid, color: Colors.white)
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                    ),
                    Text(
                      "Summary",
                      style: TextStyle(fontSize: 25.0),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                    ),
                    SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: DataTable(
                              columns: [
                                DataColumn(label: Text("Qty"), tooltip: "Quantity"),
                                DataColumn(label: Text("Item Name"), tooltip: "Item Name"),
                                DataColumn(label: Text("Type",textAlign: TextAlign.right,), tooltip: "Delivery Type"),
                              ],
                              rows: products.map((e) {
                                // double d = double.parse(e['price'].toString());
                                return DataRow(cells: [
                                  DataCell(Container(
                                    width: wa,
                                    child: Text(
                                        (e['item_uom'].toString() != "null")?e['quantity'].toString()+" "+e['item_uom'].toString():e['quantity'].toString()+" "+"N/A",
                                        style: TextStyle(fontSize: 12.5)
                                    ),
                                  )),
                                  DataCell(Container(
                                    width: wq,
                                    child: Text(
                                        (e['name'].toString() != "null")?e['name'].toString():"N/A",
                                        style: (e['name'].toString() == "Total")? TextStyle(fontSize: 13, fontWeight: FontWeight.bold) :TextStyle(fontSize: 12.5)),
                                  )),
                                  DataCell(Container(
                                    width: wt,
                                    child: Text(e['type'].toString(), style: (e['name'].toString() == "Total")? TextStyle(fontSize: 12, fontWeight: FontWeight.bold) :TextStyle(fontSize: 12.5), textAlign: TextAlign.left),
                                  ))
                                ]);
                              }).toList()),

                        )
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Notes :",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16
                      ),
                    ),
                    Text(
                      payment_term.toString(),
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16
                      ),
                    )
                  ],
                ),
                padding: const EdgeInsets.all(10.0),
                width: MediaQuery.of(context).size.width,
                height: 50.0,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black38,
                        blurRadius: 10,
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    color: Colors.white,
                    border: Border.all(
                        style: BorderStyle.solid, color: Colors.white)
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
              ),
              (widget.status == 'delivered')?
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
              ) :
              TextField(
                controller: notes,
                maxLines: 8,
                decoration: InputDecoration(
                  hintText: "Notes Approval...",
                  border: new OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(0.0),
                    ),
                    borderSide: new BorderSide(
                      color: Colors.black,
                      width: 1.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}

class getDtl {
  getDtl(this.id, this.qty, this.price, this.item_name, this.item_id, this.item_uom);

  // ignore: non_constant_identifier_names
  final int id;
  // ignore: non_constant_identifier_names
  final String qty;

  // ignore: non_constant_identifier_names
  final String item_id;
  final String item_uom;

  final String price;
  // ignore: non_constant_identifier_names
  final String item_name;
}
