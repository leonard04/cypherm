import 'package:cool_alert/cool_alert.dart';
import 'package:cypherm/DoDtlPage.dart';
import 'package:cypherm/modal/DoList.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'util/const.dart';


class DoAll extends StatefulWidget {
  String ip = "";
  String companyId = "";
  DoAll({Key key, this.ip, this.companyId, int id}) : super(key: key);

  @override
  _DoAllState createState() => _DoAllState();
}

class _DoAllState extends State<DoAll> {
  final listAll = new List<DoRequest>();
  String project = "cypher4";
  var loading = false;

  @override
  void initState() {
    super.initState();
    _lihatDataAll();

  }

  _lihatDataAll() async {
    // print("ip: http://" + widget.ip + "/"+project+"/public/api/do/" + widget.companyId);
    listAll.clear();
    setState(() {
      loading = true;
    });
    var url =
        "http://" + widget.ip + "/"+project+"/public/api/do/" + widget.companyId;

    final response = await http.get(url);
    if (response.contentLength == 2) {
    } else {
      final data = jsonDecode(response.body)['data'];
      data.forEach((api) {
        final ab = new DoRequest(api['id'], api['no_do'], api['division'],
          api['deliver_by'],api['nameFrom'], api['nameTo'],api['company_id'],widget.ip, api['approved_by'], api['approved_time'],);
        listAll.add(ab);
      });
      setState(() {
        loading = false;
      });
      // print(listAll.length);
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading
          ? Center(child: CircularProgressIndicator())
          : (listAll.length <= 0) ?
      Container(
        padding: EdgeInsets.all(10),
        child: GestureDetector(
          onTap: (){

            // print("Tap "+poItem.ip);
          },
          child: Card(
            elevation: 10,
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                child: ListTile(
                  leading: Container(
                    width: 10.0,
                    color: Colors.black12,
                  ),
                  title:  Text(
                    "No Record Found",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        fontSize: 18,
                        fontStyle: FontStyle.italic
                    ),
                  ),
                  subtitle:  Text(
                    "",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontSize: 16
                    ),
                  ),

                )
            ),
          ),
        ),

      ) :
      ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: listAll.length,
        itemBuilder: (BuildContext context, int index){
          DoRequest doList = listAll[index];
          return Container(
            padding: EdgeInsets.all(10),
            child: GestureDetector(
              onTap: (){
                if(doList.approved_by.toString() == "null"){
                  // print("Belum Diapprove");

                  Navigator.push(context, new MaterialPageRoute(
                      builder: (context) => DoDtl(
                        ip: widget.ip,
                        companyId: doList.company_id.toString(),
                        id: doList.id,
                        do_num: doList.no_do.toString(),
                        reload: _lihatDataAll,
                        status: "waiting",
                      ))
                  );
                } else {
                  Navigator.push(context, new MaterialPageRoute(
                      builder: (context) => DoDtl(
                        ip: widget.ip,
                        companyId: doList.company_id.toString(),
                        id: doList.id,
                        do_num: doList.no_do.toString(),
                        reload: _lihatDataAll,
                        status: "delivered",
                      ))
                  );
                  // CoolAlert.show(
                  //   context: context,
                  //   type: CoolAlertType.info,
                  //   text: "This DO has been approved!",
                  // );
                }
                // print("Tap "+poItem.ip);
              },
              child: Card(
                elevation: 10,
                child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                    child: ListTile(
                      leading: Container(
                        width: 10.0,
                        color: (doList.approved_by.toString() == "null") ? Colors.amber : Colors.greenAccent,
                      ),
                      title:  Text(
                        (doList.no_do.toString() != "null")?doList.no_do.toString():"N/A",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 18
                        ),
                      ),
                      subtitle:  Text(
                        (doList.nameFrom.toString() != "null")?'From '+doList.nameFrom.toString()+' to '+doList.nameTo.toString():"-",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 16
                        ),
                      ),

                    )
                ),
              ),
            ),

          );
        },
      ),
    );
  }



}