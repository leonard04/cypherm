import 'dart:convert';
import 'dart:async';
import 'package:cypherm/AddActivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import './modal/SessionPage.dart';
import 'package:http/http.dart' as http;
import 'package:cool_alert/cool_alert.dart';
import 'package:progress_dialog/progress_dialog.dart';
import "package:intl/intl.dart";
import 'package:html/parser.dart';

import 'modal/UserActivityList.dart';

// ignore: must_be_immutable
class UserActDtl extends StatefulWidget {
  // ignore: non_constant_identifier_names
  String ip, companyId,username_widget,id;

  // ignore: non_constant_identifier_names
  UserActDtl({Key key, this.ip, this.companyId,this.username_widget, this.id})
      : super(key: key);

  @override
  _UserActDtlState createState() => _UserActDtlState();
}

class _UserActDtlState extends State<UserActDtl> {
  SessionManager pref = SessionManager();
  ProgressDialog pr;
  final list = new List<getDtl>();
  var loading = false;

  String _parseHtmlString(String htmlString) {
    final document = parse(htmlString);
    final String parsedString = parse(document.body.text).documentElement.text;

    return parsedString;
  }
  String username_api = "", email = "", name = "", username="";
  final user_activity = new List<UserActRequest>();

  Future _ambilDtl() async {
    loading = true;
    list.clear();
    user_activity.clear();
    // String project = "ritz";
    String project = "cypher4";
    try {
      String url = "http://" +
          widget.ip +
          "/"+project+"/public/api/users/activity/" +
          widget.companyId +
          "/" +
          widget.id;
      final response = await http.get(url);
      if (response.contentLength == 2) {
      } else {
        list.clear();
        final data = jsonDecode(response.body)['data'];
        //print(data);
        // print(data['po_detail']);
        setState(() {
          name = data['user']['name'].toString();
          email = data['user']['email'].toString();
          username_api = data['user']['username'].toString();
        });


        var d2 = data['user_activity'];
        for (final i in d2) {
          final ab = new UserActRequest(i['id'], i['user_id'], i['location'], i['notes'], i['latitude'],i['longitude'],i['created_at'], widget.ip);

          user_activity.add(ab);
        }

        print(user_activity);
        setState(() {
          loading = false;
        });

      }

    } catch (e) {}
  }

  @override
  void initState() {
    Future<String> authToken = pref.getUsername();
    authToken.then((data) {
      setState(() {
        username = data.toString();
      });
    }, onError: (e) {
      print(e);
    });
    _ambilDtl();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, showLogs: true);
    pr.style(message: 'Please wait...');
    pr = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);


    return Scaffold(
      // backgroundColor: Color.fromRGBO(252, 241, 241, 1),
        appBar: new AppBar(
          leading: IconButton(
            icon: Icon(CupertinoIcons.back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          //warna background
          toolbarHeight: 70.0,
          backgroundColor: Colors.orange[600],
          //judul
          centerTitle: false, // this is all you need
          title: new Text("User Activity\n" + name +" - (" +widget.username_widget.toString()+")"),

          //bottom
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(30.0),
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: 100.0,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black38,
                        blurRadius: 10,
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    color: Colors.white,
                    border: Border.all(
                        style: BorderStyle.solid, color: Colors.white)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        children: [
                          Text(
                            "Name:",
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                          ),
                          Text(
                            (name.toString() != "null")?name.toString():"N/A",
                            style:
                            TextStyle(color: Colors.black, fontSize: 25.0),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(right: 10.0),
                      width: 140.0,
                      height: 50.0,
                      child: RaisedButton(
                        color: Color(0xFF005377),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        ),
                        child: Text(
                          "Add Activity",
                          style: TextStyle(color: CupertinoColors.white, fontSize: 18.0),
                        ),
                        onPressed: () {
                          Navigator.push(context, new MaterialPageRoute(
                              builder: (context) => AddActivity(
                                ip: widget.ip,
                                id: widget.id,
                                companyId: widget.companyId,
                                name: name,
                                reload: _ambilDtl,
                                user_id: widget.id,
                                username_widget: widget.username_widget.toString(),
                              ))
                          );

                        },
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
              ),
              Container(
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black38,
                        blurRadius: 10,
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    color: Colors.white,
                    border: Border.all(
                        style: BorderStyle.solid, color: Colors.white)
                ),
                padding: const EdgeInsets.all(10.0),
                width: MediaQuery.of(context).size.width,

                child: Column(
                  children: [
                    Text(
                      "Information Detail",
                      style: TextStyle(fontSize: 25.0),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 1.0),
                    ),
                    ListTile(
                      leading: Icon(CupertinoIcons.person_crop_rectangle_fill),
                      title: Text(
                        "Username: "+username_api.toString(),
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      subtitle: Text(
                        "Email: "+email.toString(),
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                        ),
                      ),

                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
              ),

              Text(
                "Activity",
                style: TextStyle(fontSize: 25.0),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 1.0),
              ),

              loading
                  ? Center(child: CircularProgressIndicator())
                  : (user_activity.length <= 0)?
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(10),
                child: GestureDetector(
                  onTap: (){

                    // print("Tap "+poItem.ip);
                  },
                  child: Card(
                    elevation: 10,
                    child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                        child: ListTile(
                          leading: Container(
                            width: 10.0,
                            color: Colors.black12,
                          ),
                          title:  Text(
                            "No Record Found",
                            textAlign: TextAlign.right,
                            style: TextStyle(
                                fontSize: 18,
                                fontStyle: FontStyle.italic
                            ),
                          ),
                          subtitle:  Text(
                            "",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontSize: 16
                            ),
                          ),

                        )
                    ),
                  ),
                ),

              ) :
              ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: user_activity.length,
                itemBuilder: (BuildContext context, int index){
                  UserActRequest userAct = user_activity[index];
                  return Container(
                    padding: EdgeInsets.all(10),
                    child: GestureDetector(
                      onTap: (){

                        // print("Tap "+poItem.ip);
                      },
                      child: Card(
                        elevation: 10,
                        child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                            child: ListTile(
                              leading: Container(
                                width: 10.0,
                                color: Color(0xFF06A77D),
                              ),
                              title:  Text(
                                "Latitude: "+userAct.latitude.toString() +"\nLongitude: "+userAct.longitude.toString()+"\n",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 18
                                ),
                              ),
                              subtitle:  Text(
                                "Address: "+userAct.location.toString()+"\n\nNotes: "+userAct.notes.toString()+"\n\nTime: "+DateFormat.yMMMEd().format(DateFormat("yyyy-MM-dd hh:mm:ss").parse(userAct.created_at.toString()))+" "+DateFormat.jm().format(DateFormat("yyyy-MM-dd hh:mm:ss").parse(userAct.created_at.toString())),
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 16
                                ),
                              ),
                              isThreeLine: true,
                            )
                        ),
                      ),
                    ),

                  );
                },
              ),

              Padding(
                padding: const EdgeInsets.only(top: 20.0),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
              ),


            ],
          ),
        )
    );
  }
}
class getDtl {
  getDtl(this.id, this.location, this.latitude, this.longitude);

  // ignore: non_constant_identifier_names
  final int id;
  // ignore: non_constant_identifier_names
  final String location;

  // ignore: non_constant_identifier_names
  final String latitude;
  final String longitude;

}