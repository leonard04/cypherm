import 'dart:async';

import 'package:cypherm/DoHome.dart';
import 'package:cypherm/LeadsPage.dart';
import 'package:cypherm/UserActDtl.dart';
import 'package:cypherm/main.dart';
import 'package:cypherm/modal/UserList.dart';
import 'package:cypherm/util/choices.dart';
import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart' as geo;
import 'package:geocoding/geocoding.dart' as _geolocator;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:location/location.dart' as loc;
import './modal/SessionPage.dart';
import 'package:flutter/material.dart';
import 'PoPage.dart';
import 'WoPage.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

ProgressDialog pr;
String username, companyId, ipLocal, idUser;

class HomePage extends StatefulWidget {
  String ip = "";
  String companyId = "";
  HomePage({Key key, this.ip, this.companyId, int id}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  MapController _controller = new MapController();
  @override
  void dispose() {
    super.dispose();
  }
  var locationMessage = "";
  var latitude = "";
  var longitude = "";
  String _address = "";

  final loc.Location location = loc.Location();

  loc.PermissionStatus _permissionGranted;
  Future<void> _checkPermissions() async {
    final loc.PermissionStatus permissionGrantedResult =
        await location.hasPermission();
    setState(() {
      _permissionGranted = permissionGrantedResult;
    });
  }

  void _getPlace() async {
    List<_geolocator.Placemark> newPlace =
        await _geolocator.placemarkFromCoordinates(
            double.parse(latitude), double.parse(longitude));

    // this is all you need
    _geolocator.Placemark placeMark = newPlace[0];
    String street = placeMark.street;
    String subLocality = placeMark.subLocality;
    String locality = placeMark.locality;
    String subadministrativeArea = placeMark.subAdministrativeArea;
    String administrativeArea = placeMark.administrativeArea;
    String postalCode = placeMark.postalCode;
    String country = placeMark.country;
    String address =
        "${street}, ${subLocality}, ${locality}, ${subadministrativeArea}, ${administrativeArea} ${postalCode}, ${country}";

    // print(address);

    setState(() {
      _address = address; // update _address
    });
    print(_address);
  }

  Future<void> _requestPermission() async {
    if (_permissionGranted != loc.PermissionStatus.granted) {
      final loc.PermissionStatus permissionRequestedResult =
          await location.requestPermission();
      setState(() {
        _permissionGranted = permissionRequestedResult;
      });
    }
  }

  void getCurrentLocation() async {
    var position = await geo.Geolocator().getCurrentPosition(
        desiredAccuracy: geo.LocationAccuracy.bestForNavigation);
    var lastPosition = await geo.Geolocator().getLastKnownPosition();
    setState(() {
      latitude = position.latitude.toString();
      longitude = position.longitude.toString();
    });
    _getPlace();
    // print(_address);
    print(latitude);
    print(longitude);

  }

  SessionManager pref = SessionManager();
  //create controller untuk tabBar
  TabController controller;
  // shared preference
  String username = "";
  String idUser = "";
  String ipLocal = "";
  String companyId = "";
  String companyName = "";
  String project = "cypher4";
  // String project = "ritz";

  var loading = false;
  final list = new List<UserRequestList>();

  _lihatData(String ip, String compId) async {
    list.clear();
    setState(() {
      loading = true;
    });
    var url =
        "http://" + ip + "/"+project+"/public/api/users/" + compId;
    final response = await http.get(url);
    if (response.contentLength == 2) {
    } else {
      final data = jsonDecode(response.body)['data'];
      data.forEach((api) {
        final ab = new UserRequestList(api['id'], api['company_id'],api['name'],api['email'], api['username'],widget.ip);
        list.add(ab);
      });
      setState(() {
        loading = false;
      });
    }
    print(list);
  }

  var i2;
  @override
  void initState() {
    _checkPermissions();
    _permissionGranted == loc.PermissionStatus.granted
        ? null
        : _requestPermission();

    pref.getCompanyName().then((String result) {
      setState(() {
        companyName = result;
      });
    });

    pref.getidUser().then((String result){
      setState(() {
        idUser = result;
        // print(idUser);
      });
    });

    Future<String> authToken = pref.getUsername();
    authToken.then((data) {
      setState(() {
        username = data.toString();
      });
    }, onError: (e) {
      print(e);
    });
    Future<String> xxxx = pref.getIp();
    xxxx.then((data) {
      setState(() {
        ipLocal = data.toString();
      });
    }, onError: (e) {
      print(e);
    });

    Future<String> xCID = pref.getCompanyId();
    xCID.then((data) {
      setState(() {
        companyId = data.toString();
      });
    }, onError: (e) {
      print(e);
    });
    // _lihatData();
    // print(pref.getIp());
    pref.getIp().then((String resultIp) {
      pref.getCompanyId().then((String resultCompId){
        _lihatData(resultIp, resultCompId);
      });
    });

    super.initState();
    // print(ipLocal);
  }

  createDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            scrollable: true,
            title: Text("Menu"),
            content: Container(
              height: MediaQuery.of(context).size.height - 400,
              width: MediaQuery.of(context).size.width,
              child: Wrap(
                spacing: 20,
                runSpacing: 20,
                alignment: WrapAlignment.spaceBetween,

                children: <Widget>[
                  createMenuPO("Purchase Order", "", "add_shopping_cart",90.0),
                  createMenuWO("Work Order", "", "add_shopping_cart",90.0),
                  // createMenusLeads("Leads", "", "add_shopping_cart",90.0),
                  createMenuDO("Delivery Order",90.0),
                  // createMenuPO("Purchase Order", "", "add_shopping_cart",90.0),
                  // createMenuWO("Work Order", "", "add_shopping_cart",90.0),
                  // createMenusLeads("Leads", "", "add_shopping_cart",90.0),
                  // createMenuDO("Delivery Order",90.0),
                ],
               
              ),
            ),
          );
        });
  }

  Container createMenuDO(String nama,double height) {
    i2 = CupertinoIcons.airplane;

    return Container(
      height: height,
      child: Column(
        children: [
          RaisedButton(
              color: CupertinoColors.activeOrange,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular((20.0)))),
              child: Icon(
                i2,
                size: 30.0,
              ),
              onPressed: () {
                // print(ipLocal);
                // print(companyId);
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => DOHomePage(
                              ip: ipLocal,
                              companyId: companyId,
                            )));
              }),
          Text(nama)
        ],
      ),
    );
  }

  Container createMenuWO(String nama, String url, String icons,double height) {
    if (icons == "add_shopping_cart") {
      i2 = CupertinoIcons.briefcase;
    }
    return Container(
        height: height,
        child: Column(
      children: [
        RaisedButton(
          color: CupertinoColors.activeOrange,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
          ),
          child: Icon(
            i2,
            // color: Colors.white,
            size: 30.0,
          ),
          onPressed: () {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => WoList(
                          ip: ipLocal,
                          companyId: companyId,
                        )));
          },
        ),
        Text(nama)
      ],
    ));
  }

  Container createMenuPO(String nama, String url, String icons,double height) {
    if (icons == "add_shopping_cart") {
      i2 = CupertinoIcons.shopping_cart;
    }
    return Container(
        child: Column(
      children: [
        RaisedButton(
          color: Colors.orange[600],
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
          ),
          child: Icon(
            i2,
            // color: Colors.white,
            size: 30.0,
          ),
          onPressed: () {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => PoList(
                          ip: ipLocal,
                          companyId: companyId,
                        )));
          },
        ),
        Text(nama)
      ],
    ));
  }

  Container createMenusLeads(String nama, String url, String icons,double height) {
    if (icons == "add_shopping_cart") {
      i2 = CupertinoIcons.personalhotspot;
    }
    return Container(
        height: height,
        child: Column(
      children: [
        RaisedButton(
          color: Colors.orange[600],
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
          ),
          child: Icon(
            i2,
            // color: Colors.white,
            size: 30.0,
          ),
          onPressed: () {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => LeadsList(
                          ip: ipLocal,
                          companyId: companyId,
                        )));
          },
        ),
        Text(nama)
      ],
    ));
  }

  Future choiceAction(String choice) {
    // print(choice);
    switch (choice) {
      case "Sign out":
        pref.setLogout();
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (BuildContext context) => MyHomePage()),
            (route) => false);
        break;
      default:
        print(choice);
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        toolbarHeight: 70.0,
        backgroundColor: Colors.orange[600],
        centerTitle: true,
        title: new Text(companyName),
        actions: <Widget>[
          PopupMenuButton<String>(
            icon: Icon(CupertinoIcons.settings),
            onSelected: choiceAction,
            itemBuilder: (BuildContext context) {
              return Choices.choices.map((String choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: loading
          ? Center(child: CircularProgressIndicator())
          : (list.length <= 0)?
      Container(
        padding: EdgeInsets.all(10),
        child: GestureDetector(
          onTap: (){

            // print("Tap "+poItem.ip);
          },
          child: Card(
            elevation: 10,
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                child: ListTile(
                  leading: Container(
                    width: 10.0,
                    color: Colors.black12,
                  ),
                  title:  Text(
                    "No Record Found",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        fontSize: 18,
                        fontStyle: FontStyle.italic
                    ),
                  ),
                  subtitle:  Text(
                    "",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontSize: 16
                    ),
                  ),

                )
            ),
          ),
        ),

      ) :
      ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: list.length,
        itemBuilder: (BuildContext context, int index){
          UserRequestList userList = list[index];
          return (userList.username.toString() != "cypher")
          ? Container(
            padding: EdgeInsets.all(10),
            child: GestureDetector(
              onTap: (){
                // print(userList.id.toString());
                Navigator.push(context, new MaterialPageRoute(
                    builder: (context) => UserActDtl(
                      ip: ipLocal,
                      companyId: userList.company_id.toString(),
                      id: userList.id.toString(),
                      username_widget: userList.username,
                      // reload: _lihatData(ipLocal,companyId),
                    ))
                );
                // print("Tap "+poItem.ip);
              },
              child: Card(
                elevation: 10,
                child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                    child: ListTile(
                      leading: Container(
                        width: 10.0,
                        color: Color(0xFF005377),
                      ),
                      title:  Text(
                        userList.name.toString(),
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 18
                        ),
                      ),
                      subtitle:  Text(
                        "Username: "+userList.username.toString(),
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 16
                        ),
                      ),

                    )
                ),
              ),
            ),

          ): Container();
        },
      ),
      floatingActionButton: Container(
        height: MediaQuery.of(context).size.width * 0.19,
        width: MediaQuery.of(context).size.width * 0.19,
        child: FloatingActionButton(
          // backgroundColor: Colors.white,
          onPressed: () {
            createDialog(context);
          },
          child: Icon(
            CupertinoIcons.app_badge,
            // color: Colors.blue,
            size: 40.0,
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        // color: Colors.blueAccent,
        child: Container(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 50.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(
                  CupertinoIcons.home,
                  // color: Colors.white,
                  size: 50.0,
                ),
                Icon(
                  CupertinoIcons.person_crop_circle,
                  size: 50.0,
                  // color: Colors.white,
                )
              ],
            ),
          ),
          height: 80,
        ),
      ),
    );
  }
}
