class Choices{
  static const String Settings = 'Settings';
  static const String Options = 'Options';
  static const String Logout = 'Sign out';

  static const List<String> choices = <String>[
    Settings,
    Options,
    Logout
  ];
}