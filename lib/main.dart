import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import './HomePage.dart';
import './modal/SessionPage.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:cypherm/util/const.dart';
import 'package:location/location.dart' as loc;


void main() {
  runApp(new MyApp());
}

ProgressDialog pr;
String username, companyId, ipLocal, idUser, company_name;

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: Constants.appName,
      theme: Constants.lightTheme,
      darkTheme: Constants.darkTheme,
      home: new MyHomePage(),
      routes: <String, WidgetBuilder>{
        //username: username, idUser: idUser
        '/HomePage': (BuildContext context) => new HomePage(),
        //'/AdminPage': (BuildContext context) => new AdminPage(username: username, idUser: idUser),
        //'/MemberPage': (BuildContext context) => new MemberPage(username: username,),
        '/MyHomePage': (BuildContext context) => new MyHomePage(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController user = new TextEditingController();
  TextEditingController pass = new TextEditingController();
  TextEditingController dedicatedip = new TextEditingController();
  // var ip = "192.168.88.216";
  var ip = "cypher.vesselholding.com";
  var loading = false;
  String project = "cypher4";
  String msg = '';
  String _valmp = 'Choose Company';
  List dx = List();
  String _valCompany;

  loc.Location location =
      loc.Location(); //explicit reference to the Location class
  Future _checkGps() async {
    if (!await location.serviceEnabled()) {
      location.requestService();
    }
  }

  @override
  void initState() {
    super.initState();
    location.serviceEnabled();
    _checkGps();
    getCompany();
    setState(() {
      // dedicatedip.text = "192.168.88.216";
      // dedicatedip.text = "kaliberindonesia.com";
      user.text = "rudi";
      pass.text = "pru9111";
      // user.text = "admin";
      // pass.text = "4dmin12iTZ";
    });
  }

  Future getCompany() async {
    dx.clear();
    setState(() {
      loading = true;
    });

    // var ip =  "192.168.88.216";
    // var ip = "kaliberindonesia.com";
    var url = "http://" + ip + "/" + project + "/public/api/company";
    final response = await http.get(url);
    if (response.contentLength == 2) {
    } else {
      var data = jsonDecode(response.body);
      setState(() {
        dx = data['data'];
        loading = false;
      });
    }
    // print(dx);
  }

  Future _login() async {
    // String project = "ritz";
    String project = "cypher4";
    await pr.show();
    SessionManager pref = SessionManager();
    String errorLogA = "", errorLogB = "";
    if (user.text == '') {
      if (user.text == '') {
        errorLogA = "Username Cannot Be Empty,";
      }
      // if (dedicatedip.text == '') {
      //   errorLogB = "Dedicated Ip Cannot Be Empty";
      // }
      CoolAlert.show(
        context: context,
        type: CoolAlertType.error,
        text: errorLogA + " " + errorLogB,
      );
    } else {
      String url = "http://" + ip + "/" + project + "/public/api/login";
      try {
        // print(url);
        // print(_valCompany);
        if (_valCompany == null) {
          final response = await http.post(url, body: {
            "username": user.text.replaceAll(' ', ''),
            "password": pass.text.replaceAll(' ', ''),
            "company": "1"
          });

          final datauser = json.decode(response.body);
          if (datauser.length == 0) {
            setState(() {
              msg = "Login Error";
            });
          } else {
            if (datauser['succes'] == 0) {
              await pr.hide();
              setState(() {
                msg = "Login Not Found!!!";
              });
            } else {
              await pr.hide();
              Navigator.pushReplacementNamed(context, '/HomePage');
              setState(() {
                username = datauser['data']['username'];
                idUser = datauser['data']['id'].toString();
                ipLocal = ip;
                companyId = datauser['data']['company_id'].toString();
                company_name = datauser['data']['company_name'].toString();
                pref.setUsername(username);
                pref.setidUser(idUser);
                pref.setIp(ipLocal);
                pref.setCompanyId(companyId);
                pref.setCompanyName(company_name);
              });
            }
          }
          return datauser;

        } else {
          final response = await http.post(url, body: {
            "username": user.text.replaceAll(' ', ''),
            "password": pass.text.replaceAll(' ', ''),
            "company": _valCompany
          });

          final datauser = json.decode(response.body);
          if (datauser.length == 0) {
            setState(() {
              msg = "Login Error";
            });
          } else {
            if (datauser['succes'] == 0) {
              await pr.hide();
              setState(() {
                msg = "Login Not Found!!!";
              });
            } else {
              await pr.hide();
              Navigator.pushReplacementNamed(context, '/HomePage');
              setState(() {
                username = datauser['data']['username'];
                idUser = datauser['data']['id'].toString();
                ipLocal = ip;
                companyId = datauser['data']['company_id'].toString();
                company_name = datauser['data']['company_name'].toString();
                pref.setUsername(username);
                pref.setidUser(idUser);
                pref.setIp(ipLocal);
                pref.setCompanyId(companyId);
                pref.setCompanyName(company_name);
              });
            }
          }
          return datauser;
        }
      } catch (e) {
        await pr.hide();
        await CoolAlert.show(
          context: context,
          type: CoolAlertType.error,
          text: "invalid Url....",
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, showLogs: true);
    pr.style(message: 'Please wait...');
    pr = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);
    final data = MediaQuery.of(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(""),
      ),
      body: loading
          ? Center(child: CircularProgressIndicator())
          : Container(
              child: Container(
              child: ListView(
                padding: const EdgeInsets.all(30.0),
                children: <Widget>[
                  Stack(
                    alignment: AlignmentDirectional.bottomCenter,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 100.0),
                          ),
                          Container(
                            width: data.size.width - 50,
                            height: 100,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('img/logo.png')),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 30.0),
                          ),
                          // TextField(
                          //   controller: dedicatedip,
                          //   decoration: InputDecoration(
                          //     hintText: "IP...",
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(top: 10.0),
                          // ),
                          TextField(
                            controller: user,
                            decoration: InputDecoration(
                              icon: Icon(
                                CupertinoIcons.person,
                                // color: Colors.black,
                              ),
                              hintText: "Username",
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                          ),
                          TextField(
                            controller: pass,
                            obscureText: true,
                            decoration: InputDecoration(
                                icon: Icon(
                                  CupertinoIcons.lock_circle,
                                ),
                                hintText: "Password"),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 25.0),
                          ),
                          DropdownButton(
                            isExpanded: true,
                            hint: Text(
                              "Vessel Group Indonesia",
                              style: TextStyle(color: Colors.black),
                            ),
                            value: _valCompany,
                            items: dx.map((item) {
                              return DropdownMenuItem(
                                child: Text(
                                  item['company_name'].toString(),
                                  style: TextStyle(color: Colors.black),
                                ),
                                value: item['id'].toString(),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                _valCompany = value;
                                // print(_valCompany);
                              });
                            },
                          ),
                          Container(
                            width: data.size.width - 50,
                            height: 50,
                            margin: const EdgeInsets.only(top: 50.0),
                            child: FlatButton(
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                              ),
                              onPressed: () {
                                _login();
                                // Navigator.pushReplacementNamed(context, '/AdminPage');
                              },
                              color: Colors.orange[400],
                              child: Text(
                                "Sign In",
                                style: TextStyle(
                                  // color: Colors.white,
                                  fontFamily: 'Raleway',
                                  fontSize: 22.0,
                                ),
                              ),
                            ),
                          ),
                          Text(
                            msg,
                            style: TextStyle(
                              fontSize: 20.0,
                              // color: Colors.red
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            )),
    );
  }
}
