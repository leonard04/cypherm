class DoRequest {
  // ignore: non_constant_identifier_names
  final int id,company_id;

  // ignore: non_constant_identifier_names
  final String no_do, division, deliver_by, nameFrom, nameTo,approved_by,approved_time,ip;

  DoRequest(this.id, this.no_do, this.division, this.deliver_by,
      this.nameFrom, this.nameTo, this.company_id, this.ip, this.approved_by, this.approved_time);
}