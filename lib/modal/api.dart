class BaseUrl {
  //local
  static String project = "ritz";
  // static String project = "cypher4";
  static String iP = "192.168.88.216";
  static String logiN = "http://" + iP + "/"+ project +"/public/api/login";
  static String inboundRegister = "http://" + iP + "/tpds/public/api/inbound";
  static String inboundListPage =
      "http://" + iP + "/tpds/public/api/getInboundList/?iP=" + iP;
  static String inboundListDtlPage =
      "http://" + iP + "/tpds/public/api/inbound2";
  static String inboundPutAway = "http://" + iP + "/tpds/public/api/putaway";
  static String outboundPickList =
      "http://" + iP + "/tpds/public/api/getPickList";
  static String outboundPickListDtl =
      "http://" + iP + "/tpds/public/api/getPickListDtl?iP=" + iP;
  static String outboundStaging = "http://" + iP + "/tpds/public/api/staging";
  static String outboundPackagingList =
      "http://" + iP + "/tpds/public/api/packagingList";
  static String outboundPackagingCek =
      "http://" + iP + "/tpds/public/api/packagingCek";
  static String outboundpackagingAdd =
      "http://" + iP + "/tpds/public/api/packagingAdd";
  static String shippingList = "http://" + iP + "/tpds/public/api/shippingList";
  static String shippingCek = "http://" + iP + "/tpds/public/api/shippingCek";
  //public
  //static String Login = "http://dsapsa.xyz/api/apiusers";
  //String inboundRegister = "http://dsapsa.xyz/api/inbound";
  //static String listPage = "http://dsapsa.xyz/api/getInboundList?iP=public";
  //static String inboundListDtlPage = "http://dsapsa.xyz/api/inbound2";
  //static String inboundPutAway = "http://dsapsa.xyz/api/putaway";
  //static String outboundPickList = "http://dsapsa.xyz/api/getPickList";
  //static String outboundPickListDtl = "http://dsapsa.xyz/api/getPickListDtl?iP=" + iP;
  //static String outboundStaging = "http://dsapsa.xyz/api/staging";
  //static String outboundPackagingList = "http://dsapsa.xyz/api/packagingList";
  //static String outboundPackagingCek = "http://dsapsa.xyz/api/packagingCek";
  //static String outboundpackagingAdd = "http://dsapsa.xyz/api/packagingAdd";
  //static String shippingList = "http://dsapsa.xyz/api/shippingList";
  //static String shippingCek = "http://dsapsa.xyz/api/shippingCek";
}
