import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class SessionManager {
  //final String username = "";
  //final int idUser = 0;

  //set data into shared preferences like this
  Future<void> setUsername(String username) async {
    //SharedPreferences.setMockInitialValues({}); //set values here
    final SharedPreferences pref = await SharedPreferences.getInstance();
    // print(username);
    await pref.setString('username', username);
  }

  //set data into shared preferences like this
  Future<void> setCompanyName(String company_name) async {
    //SharedPreferences.setMockInitialValues({}); //set values here
    final SharedPreferences pref = await SharedPreferences.getInstance();
    // print(username);
    await pref.setString('company_name', company_name);
  }

  // company id set
  Future<void> setCompanyId(String companyId) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString('companyId', companyId);
  }

  Future<void> setidUser(String idUser) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString('idUser', idUser);
  }

  Future<void> setIp(String ipLocal) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString('ipLocal', ipLocal);
  }

  // log out
  Future<void> setLogout() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.clear();
  }

  //get value from shared preferences

  //get CompanyId
  Future<String> getCompanyId() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String companyId = pref.getString('companyId') ?? null;
    return companyId;
  }

  Future<String> getUsername() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String username = pref.getString('username') ?? null;
    return username;
  }

  Future<String> getCompanyName() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String username = pref.getString('company_name') ?? null;
    return username;
  }

  Future<String> getIp() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String ipLocal = pref.getString('ipLocal') ?? null;
    return ipLocal;
  }

  Future<String> getidUser() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    String idUser = pref.getString('idUser') ?? null;
    return idUser;
  }
}
