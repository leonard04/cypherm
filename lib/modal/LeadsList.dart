class LeadsRequest{
  // ignore: non_constant_identifier_names
  final int id,company_id;
  // ignore: non_constant_identifier_names
  final String leads_name, description, created_by, progress, approved_at, approved_by, company_name, pic, pic_number, category_name, category_type, ip;

  LeadsRequest(this.id, this.company_id, this.leads_name, this.description, this.created_by, this.progress, this.approved_at, this.approved_by, this.company_name,
      this.pic, this.pic_number, this.category_name, this.category_type, this.ip);
}