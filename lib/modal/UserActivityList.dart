class UserActRequest {
  // ignore: non_constant_identifier_names
  final int id,user_id;

  // ignore: non_constant_identifier_names
  final String location,notes, latitude, longitude,created_at,ip;

  UserActRequest(this.id, this.user_id, this.location, this.notes, this.latitude,
      this.longitude, this.created_at, this.ip);
}