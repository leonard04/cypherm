class UserRequest {
  // ignore: non_constant_identifier_names
  final int id,company_id;

  // ignore: non_constant_identifier_names
  final String name, email, username,ip;

  UserRequest(this.id, this.company_id, this.name, this.email, this.username, this.ip);

  @override
  String toString() {
    return '"${this.id}"';
  }
}
class UserRequestList{
  // ignore: non_constant_identifier_names
  final int id,company_id;

  // ignore: non_constant_identifier_names
  final String name, email, username,ip;

  UserRequestList(this.id, this.company_id, this.name, this.email, this.username, this.ip);
}