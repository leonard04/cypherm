class PoRequest {
  // ignore: non_constant_identifier_names
  final int id,company_id;
  
  // ignore: non_constant_identifier_names
  final String po_num, supplier_name, project_name, approved_by, approved_time,ip;

  PoRequest(this.id, this.po_num, this.supplier_name, this.project_name,
      this.approved_by, this.approved_time, this.company_id, this.ip);
}
