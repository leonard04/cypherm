import 'package:cool_alert/cool_alert.dart';
import 'package:cypherm/modal/DoList.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'util/const.dart';
import 'package:cypherm/DoAll.dart' as all;
import 'package:cypherm/DoWaiting.dart' as waiting;
import 'package:cypherm/DoDelivered.dart' as delivered;

class DoList extends StatefulWidget {
  String ip = "";
  String companyId = "";
  DoList({Key key, this.ip, this.companyId, int id}) : super(key: key);

  @override
  _DoListState createState() => _DoListState();
}

class _DoListState extends State<DoList> with SingleTickerProviderStateMixin {

  TabController _controller;
  String project = "cypher4";

  @override
  void initState() {
    super.initState();
    _controller = new TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(primaryColor: Colors.orange),
      child: WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          backgroundColor: Constants.lightBG,
          appBar: AppBar(
            leading: Icon(Icons.list),
            title: Text('List Delivery Order'),
            bottom: TabBar(
              indicatorColor: Color(0xFF005377),
              labelPadding: EdgeInsets.zero,
              controller: _controller,
              tabs: <Widget>[
                new Tab(
                  child: Container(
                    constraints: BoxConstraints.expand(),
                    color: Colors.grey,
                    child: Center(
                      child: Text("All"),
                    ),
                  ),
                ),
                new Tab(
                  child: Container(
                    constraints: BoxConstraints.expand(),
                    color: Colors.yellow,
                    child: Center(
                      child: Text("Waiting"),
                    ),
                  ),
                ),
                new Tab(
                  child: Container(
                    constraints: BoxConstraints.expand(),
                    color: Colors.green,
                    child: Center(
                      child: Text("Delivered"),
                    ),
                  ),
                ),
              ],
            ),
          ),
          body: TabBarView(
            controller: _controller,
            children:  <Widget>[
              new all.DoAll(ip: widget.ip, companyId: widget.companyId),
              new waiting.DoWaiting(ip: widget.ip, companyId: widget.companyId),
              new delivered.DoDelivered(ip: widget.ip, companyId: widget.companyId),
            ],
          ),
          floatingActionButton: Container(
            height: MediaQuery.of(context).size.width * 0.19,
            width: MediaQuery.of(context).size.width * 0.19,
            child: FloatingActionButton(
              backgroundColor: Colors.orange,
              onPressed: () {
                Navigator.pop(context);
              },
              child: Icon(
                CupertinoIcons.home,
                color: Colors.black,
                size: 40.0,
              ),
            ),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
          bottomNavigationBar: BottomAppBar(
            shape: CircularNotchedRectangle(),
            // color: Colors.blueAccent,
            child: Container(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 50.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[

                  ],
                ),
              ),
              height: 80,
            ),
          ),
        ),
      ),
    );
  }
}
