import 'dart:async';
import 'dart:convert';

import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart' as geo;
import 'package:geocoding/geocoding.dart' as _geolocator;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:location/location.dart' as loc;
import './modal/SessionPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:http/http.dart' as http;

// ProgressDialog pr;
// String username, companyId, ipLocal, idUser;

class AddActivity extends StatefulWidget {
  String ip = "";
  String companyId = "";
  String name = "";
  String user_id = "";
  String username_widget = "";
  final VoidCallback reload;

  AddActivity({Key key, this.ip, this.companyId,this.name,this.reload,this.user_id,this.username_widget, String id}) : super(key: key);

  @override
  _AddActivityState createState() => _AddActivityState();
}

class _AddActivityState extends State<AddActivity> {
  ProgressDialog pr;
  var loading = false;

  @override
  void dispose() {
    super.dispose();
  }
  var locationMessage = "";
  var latitude = "";
  var longitude = "";
  String _address = "";
  TextEditingController notes = new TextEditingController();

  final loc.Location location = loc.Location();

  loc.PermissionStatus _permissionGranted;
  Future<void> _checkPermissions() async {
    final loc.PermissionStatus permissionGrantedResult =
    await location.hasPermission();
    setState(() {
      _permissionGranted = permissionGrantedResult;
    });
  }

  Future _submitData() async {
    // String project = "ritz";
    String project = "cypher4";
    await pr.show();
    String url = "http://" + widget.ip + "/"+project+"/public/api/users/add_activity";
    final response = await http.post(
      url,
      body: json.encode({
        "user_id": int.parse(widget.user_id.toString()),
        "username": widget.username_widget.toString(),
        "notes": notes.text.toString(),
        "latitude":latitude.toString(),
        "longitude":longitude.toString(),
        "address": _address.toString()
      }),
      headers: {'Content-Type': 'application/json'},
    );
    var res = json.decode(response.body);
    print(res);
    if (res['success'] == 0) {
      await CoolAlert.show(
        context: context,
        type: CoolAlertType.error,
        title: "Error",
        text: res['message'].toString(),

      );

    } else {
      await CoolAlert.show(
        context: context,
        type: CoolAlertType.success,
        title: "",
        text: res['message'].toString(),
      );
    }
    widget.reload();
    Navigator.pop(context);
    await pr.hide();
  }

  Future _getPlace() async {
    setState(() {
      loading = true;// pdate _address
    });
    List<_geolocator.Placemark> newPlace =
    await _geolocator.placemarkFromCoordinates(
        double.parse(latitude), double.parse(longitude));

    // this is all you need
    _geolocator.Placemark placeMark = newPlace[0];
    String street = placeMark.street;
    String subLocality = placeMark.subLocality;
    String locality = placeMark.locality;
    String subadministrativeArea = placeMark.subAdministrativeArea;
    String administrativeArea = placeMark.administrativeArea;
    String postalCode = placeMark.postalCode;
    String country = placeMark.country;
    String address =
        "${street}, ${subLocality}, ${locality}, ${subadministrativeArea}, ${administrativeArea} ${postalCode}, ${country}";

    // print(address);

    setState(() {
      _address = address; // u
      loading = false;// pdate _address
    });
    print(_address);
  }

  Future<void> _requestPermission() async {
    if (_permissionGranted != loc.PermissionStatus.granted) {
      final loc.PermissionStatus permissionRequestedResult =
      await location.requestPermission();
      setState(() {
        _permissionGranted = permissionRequestedResult;
      });
    }
  }

  Future getCurrentLocation() async {
    setState(() {
      loading = true;// pdate _address
    });
    var position = await geo.Geolocator().getCurrentPosition(
        desiredAccuracy: geo.LocationAccuracy.bestForNavigation);
    var lastPosition = await geo.Geolocator().getLastKnownPosition();
    setState(() {
      latitude = position.latitude.toString();
      longitude = position.longitude.toString();
      loading = false;
    });
    _getPlace();
    // print(_address);
    print(latitude);
    print(longitude);

  }


  SessionManager pref = SessionManager();
  //create controller untuk tabBar
  TabController controller;
  // shared preference
  String username = "";
  String idUser = "";
  String ipLocal = "";
  String companyId = "";
  String companyName = "";

  var i2;
  @override
  void initState() {
    _checkPermissions();
    _permissionGranted == loc.PermissionStatus.granted
        ? null
        : _requestPermission();

    pref.getCompanyName().then((String result) {
      setState(() {
        companyName = result;
      });
    });

    Future<String> authToken = pref.getUsername();
    authToken.then((data) {
      setState(() {
        username = data.toString();
      });
    }, onError: (e) {
      print(e);
    });
    Future<String> xxxx = pref.getIp();
    xxxx.then((data) {
      setState(() {
        ipLocal = data.toString();
      });
    }, onError: (e) {
      print(e);
    });

    Future<String> xCID = pref.getCompanyId();
    xCID.then((data) {
      setState(() {
        companyId = data.toString();
      });
    }, onError: (e) {
      print(e);
    });
    super.initState();
  }





  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context, showLogs: true);
    pr.style(message: 'Please wait...');
    pr = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false);

    return Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          icon: Icon(CupertinoIcons.back),
          onPressed: () => Navigator.of(context).pop(),
        ),
        toolbarHeight: 70.0,
        backgroundColor: Colors.orange[600],
        centerTitle: true,
        title: new Text("Add User Activity for "+widget.name),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 60.0),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width * 0.85,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            offset: Offset(0.0, 20.0),
                            blurRadius: 30.0,
                            color: Colors.black12)
                      ],
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(45.0)
                  ),
                  padding: EdgeInsets.all(10.0),
                  child: TextField(
                    controller: notes,
                    maxLines: 8,
                    decoration: InputDecoration(
                      
                      hintText: "Notes...",
                      border: InputBorder.none,
                      
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                ),
                (_address.toString() == "")?
                InkWell(
                  onTap: () {
                    getCurrentLocation();
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.1,
                    width: MediaQuery.of(context).size.width * 0.85,
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              offset: Offset(0.0, 20.0),
                              blurRadius: 30.0,
                              color: Colors.black12)
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(45.0)),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.1,
                          width: MediaQuery.of(context).size.width * 0.7,
                          padding:
                          EdgeInsets.symmetric(vertical: 35.0, horizontal: 80.0),
                          child: Text(
                            'Get Location',
                            style: Theme.of(context)
                                .textTheme
                                .button
                                .apply(color: Colors.white),
                          ),
                          decoration: BoxDecoration(
                              color: Color(0xFF005377),
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(95.0),
                                topLeft: Radius.circular(95.0),
                                bottomRight: Radius.circular(200),
                              )),
                        ),
                        Icon(
                          CupertinoIcons.map_pin_ellipse,
                          size: 30.0,
                        )
                      ],
                    ),
                  ),
                ):Container(),

                (_address.toString() != "")?
                Container(
                  height:  MediaQuery.of(context).size.height * 0.15 ,
                  width: MediaQuery.of(context).size.width * 0.8 ,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black38,
                          blurRadius: 10,
                        ),
                      ],
                      borderRadius: BorderRadius.all(Radius.circular(8.0)),
                      color: Colors.white,
                      border: Border.all(
                          style: BorderStyle.solid, color: Colors.white)
                  ),
                  padding: const EdgeInsets.all(10.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                        ),
                        ListTile(
                          leading: Icon(CupertinoIcons.pin_fill),
                          title: Text(
                            'Address',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          subtitle: Text(
                            (_address.toString() != "")? _address.toString() :'',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                            ),
                          ),

                        ),
                      ],
                    ),
                  ),
                ):Container(),

                Padding(
                  padding: const EdgeInsets.only(top: 40.0),
                ),
                loading == true
                    ? Center(child: CircularProgressIndicator())
                    :
                ((latitude != "") && (longitude != "")) ?
                Container(
                  height:  MediaQuery.of(context).size.height * 0.4 ,
                  width: MediaQuery.of(context).size.width * 0.7 ,
                  child:  Stack(
                      children: <Widget>[
                        FlutterMap(
                          options: MapOptions(
                            center: LatLng(double.parse(latitude),double.parse(longitude)),
                            zoom: 15.0,
                          ),
                          layers: [
                            TileLayerOptions(
                                urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                                subdomains: ['a', 'b', 'c']
                            ),
                            MarkerLayerOptions(
                              markers: [
                                Marker(
                                    width: 80.0,
                                    height: 80.0,
                                    point: LatLng(double.parse(latitude),double.parse(longitude)),
                                    builder: (ctx) =>
                                        Icon(CupertinoIcons.map_pin,color: CupertinoColors.systemRed,size: 40,)
                                ),
                              ],
                            ),
                          ],
                        ),
                      ]
                  ),
                ): Container(),
                Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                ),
                ((latitude != "") && (longitude != "")) ?
                InkWell(
                  onTap: () {
                   // print('nanti save');
                  _submitData();
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.1,
                    width: MediaQuery.of(context).size.width * 0.85,
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              offset: Offset(0.0, 20.0),
                              blurRadius: 30.0,
                              color: Colors.black12)
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(45.0)),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.1,
                          width: MediaQuery.of(context).size.width * 0.7,
                          padding:
                          EdgeInsets.symmetric(vertical: 35.0, horizontal: 80.0),
                          child: Text(
                            'Save Activity',
                            style: Theme.of(context)
                                .textTheme
                                .button
                                .apply(color: Colors.white),
                          ),
                          decoration: BoxDecoration(
                              color: Color(0xFF06A77D),
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(95.0),
                                topLeft: Radius.circular(95.0),
                                bottomRight: Radius.circular(200),
                              )),
                        ),
                        Icon(
                          CupertinoIcons.check_mark_circled,
                          size: 30.0,
                        )
                      ],
                    ),
                  ),
                ) : Container(),

                Padding(
                  padding: const EdgeInsets.only(top: 60.0),
                ),
              ]),
        ),
      ),
    );
  }
}
