import 'package:cool_alert/cool_alert.dart';
import 'package:cypherm/LeadsAddPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'modal/LeadsList.dart';

class LeadsList extends StatefulWidget {
  String ip = "";
  String companyId = "";
  LeadsList({Key key, this.ip, this.companyId, int id}) : super(key: key);

  @override
  _LeadsListState createState() => _LeadsListState();
}

class _LeadsListState extends State<LeadsList> {
  String project = "ritz";
  // String project = "cypher4";
  var loading = false;
  final list = new List<LeadsRequest>();

  _lihatData() async {
    list.clear();
    setState(() {
      loading = true;
    });
    var url =
        "http://" + widget.ip + "/"+project+"/public/api/leads/" + widget.companyId;
    final response = await http.get(url);
    if (response.contentLength == 2) {
    } else {
      final data = jsonDecode(response.body)['data'];
      data.forEach((api) {
        final ab = new LeadsRequest(api['id'],api['company_id'], api['leads_name'], api['description'],api['created_by'], api['progress'], api['approved_at'],
            api['approved_by'], api['company_name'], api['pic'], api['pic_number'], api['category_name'], api['category_type'], widget.ip);
        list.add(ab);
      });
      setState(() {
        loading = false;
      });
    }
  }

  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
    _lihatData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        leading: IconButton(
          icon: Icon(CupertinoIcons.back),
          onPressed: () => Navigator.of(context).pop(),
        ),
        //warna background
        toolbarHeight: 70.0,
        backgroundColor: Colors.orange[600],
        //judul
        centerTitle: true, // this is all you need
        title: new Text("Leads"),
        actions: <Widget>[
          IconButton(
              icon: Icon(CupertinoIcons.plus),
              onPressed: (){
                Navigator.push(context, new MaterialPageRoute(
                    builder: (context) => LeadsAdd(
                      ip: widget.ip,
                      companyId: widget.companyId,
                      reload: _lihatData,
                    )
                ));
              })
        ],

        //bottom
      ),
      body: loading
          ? Center(child: CircularProgressIndicator())
          : (list.length <= 0) ?
      Container(
        padding: EdgeInsets.all(10),
        child: GestureDetector(
          onTap: (){
            // print("Tap "+poItem.ip);
          },
          child: Card(
            elevation: 10,
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                child: ListTile(
                  leading: Container(
                    width: 10.0,
                    color: Colors.black12,
                  ),
                  title:  Text(
                    "No Record Found",
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        fontSize: 18,
                        fontStyle: FontStyle.italic
                    ),
                  ),
                  subtitle:  Text(
                    "",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontSize: 16
                    ),
                  ),

                )
            ),
          ),
        ),

      ) :
      ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: list.length,
        itemBuilder: (BuildContext context, int index){
          LeadsRequest leadsList = list[index];
          return Container(
            padding: EdgeInsets.all(10),
            child: GestureDetector(
              onTap: (){
                if(leadsList.approved_by.toString() == "null"){
                  // print("Belum Diapprove");

                  // Navigator.push(context, new MaterialPageRoute(
                  //     builder: (context) => WoDtl(
                  //       ip: widget.ip,
                  //       companyId: leadsList.company_id.toString(),
                  //       id: leadsList.id,
                  //       wo_num: leadsList.wo_num,
                  //       reload: _lihatData,
                  //     )
                  //   )
                  // );
                } else {
                  CoolAlert.show(
                    context: context,
                    type: CoolAlertType.warning,
                    text: "This Leads has been approved! See Project for details",
                  );
                }
                // print("Tap "+poItem.ip);
              },
              child: Card(
                elevation: 10,
                child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                    child: ListTile(
                      leading: Container(
                        width: 10.0,
                        color: (leadsList.approved_by.toString() == "null") ? Colors.amber : Colors.greenAccent,
                      ),
                      title:  Text(
                        leadsList.leads_name,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 18
                        ),
                      ),
                      subtitle:  Text(
                        leadsList.company_name,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 16
                        ),
                      ),

                    )
                ),
              ),
            ),

          );
        },
      ),
    );
  }

}

