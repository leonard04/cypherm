import 'package:cypherm/DoHome.dart';
import 'package:cypherm/DoPage.dart';
import 'package:cypherm/HomePage.dart';
import 'package:cypherm/LeadsPage.dart';
import 'package:cypherm/main.dart';
import 'package:cypherm/util/choices.dart';
import 'package:flutter/cupertino.dart';
import 'package:progress_dialog/progress_dialog.dart';

import './modal/SessionPage.dart';
import 'package:flutter/material.dart';
import 'package:cypherm/util/const.dart';

ProgressDialog pr;
String username, companyId, ipLocal, idUser;

class DOHomePage extends StatefulWidget {
  String ip = "";
  String companyId = "";
  DOHomePage({Key key, this.ip, this.companyId, int id}) : super(key: key);

  @override
  _DOHomePageState createState() => _DOHomePageState();
}
class _DOHomePageState extends State<DOHomePage> {
  SessionManager pref = SessionManager();
  //create controller untuk tabBar
  TabController controller;
  // shared preference
  String username = "";
  String idUser = "";
  String ipLocal = "";
  String companyId = "";
  String companyName = "";

  var i2;
  @override
  void initState() {

  }

  Future choiceAction(String choice){
    // print(choice);
    switch(choice){
      case "Sign out":
        pref.setLogout();
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (BuildContext context) => MyHomePage()),
                (route) => false
        );
        break;
      default:
        // print(choice);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          appBar: new AppBar(
            leading: new Container(),
            toolbarHeight: 70.0,
            backgroundColor: Colors.orange[600],
            centerTitle: true,
            title: Text(
                "Delivery Order"
            ),
          ),
          body: Center(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 60.0),
                ),

                InkWell(
                  onTap: () {

                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.1,
                    width: MediaQuery.of(context).size.width * 0.85,
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              offset: Offset(0.0,20.0),
                              blurRadius: 30.0,
                              color: Colors.black12
                          )
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(45.0)
                    ),
                    child: Row(
                      children:<Widget> [
                        Container(
                          height: MediaQuery.of(context).size.height * 0.1,
                          width: MediaQuery.of(context).size.width * 0.7,
                          padding: EdgeInsets.symmetric(
                              vertical: 35.0,
                              horizontal: 80.0
                          ),
                          child: Text(
                            'Create New DO',
                            style: Theme.of(context)
                                .textTheme
                                .button
                                .apply(
                                color: Colors.white
                            ),
                          ),
                          decoration: BoxDecoration(
                              color: Color(0xFF005377),
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(95.0),
                                topLeft: Radius.circular(95.0),
                                bottomRight: Radius.circular(200),

                              )
                          ),
                        ),
                        Icon(
                          CupertinoIcons.plus_bubble,
                          size: 30.0,
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 40.0),
                ),

                InkWell(
                  onTap: (){
                    // print(widget.companyId);
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => DoList(
                              ip: widget.ip,
                              companyId: widget.companyId,
                            )
                        )
                    );
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.1,
                    width: MediaQuery.of(context).size.width * 0.85,
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              offset: Offset(0.0,20.0),
                              blurRadius: 30.0,
                              color: Colors.black12
                          )
                        ],
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(45.0)
                    ),
                    child: Row(
                      children:<Widget> [
                        Container(
                          height: MediaQuery.of(context).size.height * 0.1,
                          width: MediaQuery.of(context).size.width * 0.7,
                          padding: EdgeInsets.symmetric(
                              vertical: 35.0,
                              horizontal: 80.0
                          ),
                          child: Text(
                            'Show DO List',
                            style: Theme.of(context)
                                .textTheme
                                .button
                                .apply(
                                color: Colors.white
                            ),
                          ),
                          decoration: BoxDecoration(
                              color: Color(0xFF06A77D),
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(95.0),
                                topLeft: Radius.circular(95.0),
                                bottomRight: Radius.circular(200),

                              )
                          ),
                        ),
                        Icon(
                          CupertinoIcons.list_bullet_indent,
                          size: 30.0,
                        )
                      ],
                    ),
                  ),
                ),
              ]

            ),
          ),
          floatingActionButton: Container(
            height: MediaQuery.of(context).size.width * 0.19,
            width: MediaQuery.of(context).size.width * 0.19,
            child: FloatingActionButton(
              // backgroundColor: Colors.white,
              onPressed: () {
               Navigator.pop(context);
              },
              child: Icon(
                CupertinoIcons.home,
                // color: Colors.blue,
                size: 40.0,
              ),
            ),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: BottomAppBar(
          shape: CircularNotchedRectangle(),
          // color: Colors.blueAccent,
          child: Container(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[

                ],
              ),
            ),
            height: 80,
          ),
        ),
      ),
    );
  }
}