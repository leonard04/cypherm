import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'modal/WoList.dart';
import 'WoDtlPage.dart';

class WoList extends StatefulWidget {
  String ip = "";
  String companyId = "";
  WoList({Key key, this.ip, this.companyId, int id}) : super(key: key);

  @override
  _WoListState createState() => _WoListState();
}

class _WoListState extends State<WoList> {
  // String project = "ritz";
  String project = "cypher4";
  var loading = false;
  final list = new List<WoRequest>();

    _lihatData() async {
    list.clear();
    setState(() {
      loading = true;
    });
    var url =
        "http://" + widget.ip + "/"+project+"/public/api/wo/" + widget.companyId;
    final response = await http.get(url);
    if (response.contentLength == 2) {
    } else {
      final data = jsonDecode(response.body)['data'];
      data.forEach((api) {
        final ab = new WoRequest(api['id'], api['wo_num'], api['supplier_name'],
            api['project_name'], api['approved_by'], api['approved_time'],api['company_id'],widget.ip);
        list.add(ab);
      });
      setState(() {
        loading = false;
      });
    }
  }

  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
    _lihatData();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          leading: IconButton(
            icon: Icon(CupertinoIcons.back),
            onPressed: () => Navigator.of(context).pop(),
          ),
          //warna background
          toolbarHeight: 70.0,
          backgroundColor: Colors.orange[600],
          //judul
          centerTitle: true, // this is all you need
          title: new Text("Work Order"),

          //bottom
        ),
        body: loading
            ? Center(child: CircularProgressIndicator())
            : (list.length <= 0) ?
        Container(
          padding: EdgeInsets.all(10),
          child: GestureDetector(
            onTap: (){
              // print("Tap "+poItem.ip);
            },
            child: Card(
              elevation: 10,
              child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                  child: ListTile(
                    leading: Container(
                      width: 10.0,
                      color: Colors.black12,
                    ),
                    title:  Text(
                      "No Record Found",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                          fontSize: 18,
                          fontStyle: FontStyle.italic
                      ),
                    ),
                    subtitle:  Text(
                      "",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 16
                      ),
                    ),

                  )
              ),
            ),
          ),

        ) :
        ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index){
            WoRequest woList = list[index];
            return Container(
              padding: EdgeInsets.all(10),
              child: GestureDetector(
                onTap: (){
                  if(woList.approved_by.toString() == "null"){
                    // print("Belum Diapprove");

                    Navigator.push(context, new MaterialPageRoute(
                        builder: (context) => WoDtl(
                          ip: widget.ip,
                          companyId: woList.company_id.toString(),
                          id: woList.id,
                          wo_num: woList.wo_num,
                          reload: _lihatData,
                        ))
                    );
                  } else {
                    CoolAlert.show(
                      context: context,
                      type: CoolAlertType.info,
                      text: "This WO has been approved!",
                    );
                  }
                  // print("Tap "+poItem.ip);
                },
                child: Card(
                  elevation: 10,
                  child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                      child: ListTile(
                        leading: Container(
                          width: 10.0,
                          color: (woList.approved_by.toString() == "null") ? Colors.amber : Colors.greenAccent,
                        ),
                        title:  Text(
                          (woList.wo_num.toString() != "null")?woList.wo_num.toString():"N/A",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 18
                          ),
                        ),
                        subtitle:  Text(
                          (woList.supplier_name.toString() != "null")?woList.supplier_name.toString():"-",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 16
                          ),
                        ),

                      )
                  ),
                ),
              ),

            );
          },
        ),
    );
  }
}
